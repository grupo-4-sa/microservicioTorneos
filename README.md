# Laboratorio Software Avanzado
## Proyecto final


# Correr pruebas unitarias
```javascript
mocha
```
# Ejecutar SonarQube
```javascript
sonar-scanner
```

# Creación de Artefactos
Las configuraciones se encuentran dentro del archivo gulpfile.js para crear los artefactos de cada una de las aplicaciones ejecutamos el siguiente comando
```javascript
gulp
```
Este comando creará la carpeta "dist" que contendrá el archivos comprimido que contiene el codigo fuente
- tournaments

# Herramientas
Esta aplicación fue desarrollada utilizando NodeJS y express
Para instalar los módulos necesarios corremos el siguiente comando
```javascript
npm install
```
Para ejecutar el archivo utilizamos el siguiente comando dentro de la carpeta que contiene el archivo. 
```javascript
npm start
```
# Docker 
 ## API REST
 Para la ejecución de la aplicación se creó un Dockerfile que está basado en Node y que permite instalar las dependencias necesarias.

Dockerfile:
```Dockerfile
FROM node
WORKDIR /App
ADD . /App
RUN npm install
ENV PORT 3000
CMD ["node","index.js"]

```


 ## Docker Compose
El archivoo de docker compose se compone de 2 servicios y una red interna que permite la comunicación entre todos los servicios. Los parámetros de conexión tambien se pasan por medio del docker compose. 
Dockerfile:
```yml
version: "3"
services:
  api_torneo:
    container_name: torneo
    build: './API/'
    networks:
     - testing_net
    env_file: './API/.env'
    depends_on:
      - mysql_torneo  
  mysql_torneo:
    container_name: mysql_torneo
    ports:
      - "3316:3306"
    restart: always
    build: './DB/'
    command: --default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_ROOT_PASSWORD: 123456789
      MYSQL_DATABASE: practica
    volumes:
      - my-db:/var/lib/mysql
    networks:
       - testing_net
networks:
    testing_net:
volumes:
  my-db:

```
### Autor: Luis Fernando Lizama - 201602656