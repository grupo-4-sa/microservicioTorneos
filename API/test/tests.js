const assert = require('chai').assert;
const getGameID = require('../utils').getGameID;
const shuffle = require('../utils').shuffle;


describe('GetGameID', function(){
    it('App should return 2', function(){
        let gameName = 'Parchis';
        let expected = 2;
        assert.equal(expected, getGameID(gameName));
    })
})

describe('Shuffle Array', function(){
    it('App should return a different array', function(){
        let originalArray = [1,2,3,4,5,6,7,8,9,10,11,12,13]
        let shuffleArray = [1,2,3,4,5,6,7,8,9,10,11,12,13]
        assert.notEqual(originalArray, shuffle(shuffleArray));        
    })
})
