const express = require('express');
const bodyParser = require('body-parser');
var utils = require('./utils')
var mysql = require('mysql');
var cors = require('cors')
var logger = require('morgan');

const app = express();
app.use(logger('dev'));
app.use(cors())

const ip = process.env.IP || "34.71.49.153";
const port = process.env.PORT || 3000;
const pass = process.env.PASS || "123456789";
const user = process.env.USER || "root";

//Variables de Angular
const USERS_SERVICE = process.env.USERS_SERVICE || null;
const TOURNAMENT_BACKEND = process.env.TOURNAMENT_BACKEND || null;
const IP_TOKEN = process.env.IP_TOKEN || null;
const ID_SECRET = process.env.ID_SECRET || null;
const SECRET_SECRET = process.env.SECRET_SECRET || null;
const ENCRYPT = process.env.ENCRYPT || null;




app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var connection = mysql.createPool({
    connectionLimit: 30,
    host: ip,
    port: 3306,
    user: user,
    password: pass,
    database: 'practica'
});


app.get('/', (req, res) => {
    res.status(200).send({ "message": `Todo OK` });
});

app.get('/variables', (req, res) => {
    res.status(200).send({
        USERS_SERVICE: USERS_SERVICE,
        TOURNAMENT_BACKEND: TOURNAMENT_BACKEND,
        IP_TOKEN: IP_TOKEN,
        ID_SECRET: ID_SECRET,
        SECRET_SECRET: SECRET_SECRET,
        ENCRYPT: ENCRYPT
    });
});

app.post('/torneos', (req, res) => {
    if (!req.body.nombre) {
        console.log('Se reachazo la creacion del torneo')
        res.status(400).send({ message: 'No se encontró el atributo nombre' });
    }
    else if (!req.body.id_juego) {
        console.log('Se reachazo la creacion del torneo')
        res.status(400).send({ message: 'No se encontró el atributo id_juego' });
    }
    else if (!req.body.participantes) {
        console.log('Se reachazo la creacion del torneo')
        res.status(400).send({ message: 'No se encontró el atributo participantes' });
    } else {
        console.log(`Se está creando un torneo con los valores ('${req.body.nombre}', ${req.body.id_juego}, ${req.body.participantes})`);
        connection.query(`INSERT INTO torneo(nombre, id_juego, participantes) 
                        values ('${req.body.nombre}', ${req.body.id_juego}, ${req.body.participantes})`,
            function (err) {
                if (err) {
                    res.status(500).send({ message: 'Error de conexion a BD' })
                    console.log(err);
                } else {
                    res.status(200).send({ message: 'Registro insertado correctamente' });
                }
            });
    }
});



app.get('/torneos', (req, res) => {
    console.log('Se esta seleccionando el listado de torneos')
    connection.query(`  Select t.*, e.nombre as estado, j.nombre as juego
                        from torneo  as t, estado as e, juego as j
                        where t.id_estado = e.id_estado and j.id_juego = t.id_juego;`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result);
        });

});

app.get('/torneos/:id', (req, res) => {
    console.log(`Se está seleccionando el torneo con el ID ${req.params.id}`)
    connection.query(`  Select t.*, e.nombre as estado, j.nombre as juego
                        from torneo  as t, estado as e, juego as j
                        where t.id_estado = e.id_estado and j.id_juego = t.id_juego and t.id_torneo=${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result[0]);
        });

});

app.put('/torneos/:id', (req, res) => {
    console.log(`Se están modificando los valores nombre = '${req.body.nombre}',
            id_juego = ${req.body.id_juego},
            id_ganador = ${req.body.id_ganador},
            id_estado = ${req.body.id_estado}
             del id_torneo = ${req.params.id}`)

    connection.query(` Update torneo
                       set nombre = '${req.body.nombre}',
                       id_juego = ${req.body.id_juego},
                       id_ganador = ${req.body.id_ganador},
                       id_estado = ${req.body.id_estado}
                        where id_torneo = ${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                res.status(200).send({});
            }
        });
});

app.delete('/torneos/:id', (req, res) => {
    console.log(`Se está eliminando el torneo ${req.params.id}`)
    connection.query(` delete from torneo 
                        where id_torneo = ${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                res.status(200).send(result);
            }
        });
});

app.get('/torneos/futuros/:usuario', (req, res) => {
    console.log(`Obteniendo informacion de proximos torneos del usuario ${req.params.usuario}`)
    
    const query = `call MisProximosTorneos(${req.params.usuario})`;
    connection.query(query,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result[0]);
        });
});

app.get('/torneos/pasados/:usuario', (req, res) => {
    console.log(`Obteniendo informacion de torneos pasados del usuario ${req.params.usuario}`)

    const query = `call MisTorneosPasados(${req.params.usuario})`;
    connection.query(query,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result[0]);
        });
});

app.get('/torneos/activos/:usuario', (req, res) => {
    console.log(`Obteniendo informacion de torneos activos del usuario ${req.params.usuario}`)
    const query = `call MisTorneosActivos(${req.params.usuario})`;
    connection.query(query,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result[0]);
        });
});

app.post('/torneos/registrar', (req, res) => {
    console.log(`Registrando al usuario ${req.body.id_usuario} en el torneo ${req.body.id_torneo}`)
    const query = `call InscribirParticipante(${req.body.id_usuario}, ${req.body.id_torneo})`;
    connection.query(query,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(201).send();
        });
});

app.post('/torneos/randomize', (req, res) => {
    console.log(`El torneo ${req.body.id} esta generando sus llaves aleatorias`)
    connection.query(`Select * from participante 
                      where id_torneo = ${req.body.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                let brackets = []
                brackets = utils.randomizeTournaments(result)
                brackets.forEach(match => {
                    createBrackets(match, req.body.id)
                })
                setReadyState(req.body.id)
                res.status(200).send({});
            }

        });

});

/*
########################################
Permisos
########################################
*/



app.get('/permisos/:id', (req, res) => {
    console.log(`Obteniendo los permisos del rol ${req.params.id}`)
    connection.query(`Select * from permisos
                        where id_rol=${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result);
        });
});



/*
#########################
###### PARTIDAS #########
#########################
*/
// Es un POST para no utilizar dos parámetros
app.post('/torneos/jugar', (req, res) => {
    console.log(`Obteniendo las partidas pendientes del jugador ${req.body.jugador} del torneo ${req.body.torneo}`)
    connection.query(`Select * from partida 
                    where (id_jugador1 = ${req.body.jugador} or id_jugador2 =${req.body.jugador}) 
                    and (resultado_jugador1 = 0 and resultado_jugador2 = 0) 
                    and id_torneo = ${req.body.torneo}
                    order by ronda desc;`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result);
        });
});

app.get('/partidas/:id', (req, res) => {
    console.log(`Obteniendo todas las partidas del torneo ${req.params.id}`)

    connection.query(`Select * from partida
                        where id_torneo=${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result);
        });
});

app.put('/partidas/:id', (req, res) => {
    console.log(`Registrando el marcador ${req.body.marcador} de la partida ${req.params.id}`)
    if (req.body.marcador && req.params.id) {
        if (req.body.marcador.length == 2) {
            connection.query(`call DeclararGanador(${req.body.marcador[0]}, ${req.body.marcador[1]}, '${req.params.id}')`,
                function (err, result) {
                    if (err) {
                        res.status(500).send({ message: 'Error de conexion a BD' })
                        console.log(err);
                    } else {
                        res.status(201).send();
                    }
                });
        } else {
            res.status(406).send();
        }
    } else {
        res.status(406).send();
    }
});

app.put('/registrar/:id', (req, res) => {
    console.log(`Registrando la partida ${req.params.id} en la base de datos como comenzada`)    
    let quer = `update partida
    set id_estado = 2
    where id_partida = '${req.params.id}'`
    console.log(quer)
    connection.query(quer,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                res.status(201).send();
            }
        });
});

/*#######################################*/
//############## JUEGOS #################
/*#######################################*/

app.post('/juegos', (req, res) => {
    console.log(req.body);
    if (!req.body.nombre) {
        console.log(`Se rechazo la creación del juego`)    
        res.status(400).send({ message: 'No se encontró el atributo nombre' });
    }
    else if (!req.body.direccion) {
        console.log(`Se rechazo la creación del juego`)    
        res.status(400).send({ message: 'No se encontró el atributo direccion' });
    }
    else if (!req.body.front) {
        console.log(`Se rechazo la creación del juego`)    
        res.status(400).send({ message: 'No se encontró el atributo front' });
    } else {
        console.log(`Registrando un juego con los valores ('${req.body.nombre}','${req.body.direccion}','${req.body.front}')`)    

        connection.query(`insert into juego(nombre, direccion, front) values ('${req.body.nombre}','${req.body.direccion}','${req.body.front}')`,
            function (err) {
                if (err) {
                    res.status(500).send({ message: 'Error de conexion a BD' })
                    console.log(err);
                } else {
                    res.status(200).send({ message: 'Registro insertado correctamente' });
                }
            });
    }
});


app.get('/juegos', (req, res) => {
    console.log('Obteniendo la lista de todos los juegos');
    connection.query(`select * from juego`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result);
        });

});

app.get('/juegos/:id', (req, res) => {
    console.log(`Obteniendo el juego con el id ${req.params.id}`);

    connection.query(`select * from juego where id_juego=${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            }
            res.status(200).send(result[0]);
        });

});

app.put('/juegos/:id', (req, res) => {
    console.log(`Se esta generando una petición PUT. Los valores modificados son nombre = '${req.body.nombre}', direccion='${req.body.direccion}', front='${req.body.front}
                para el juego ${req.params.id}`)    

    connection.query(`update juego
                    set nombre = '${req.body.nombre}', direccion='${req.body.direccion}', front='${req.body.front}'
                    where id_juego = ${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                res.status(200).send({});
            }
        });
});

app.delete('/juegos/:id', (req, res) => {
    console.log(`Se esta eliminando el juego con el ID ${req.params.id}`)
    connection.query(` delete from juego 
                        where id_juego = ${req.params.id}`,
        function (err, result) {
            if (err) {
                res.status(500).send({ message: 'Error de conexion a BD' })
                console.log(err);
            } else {
                res.status(200).send(result);
            }
        });
});



app.listen(port, () => {
    console.log(`API Rest corriendo en http://localhost:${port}`);

});

function createBrackets(match, id) {
    connection.query(`insert into partida (id_partida, id_torneo, id_jugador1, id_jugador2, llave_siguiente, ronda)  
                                        values('${match.id}', ${id}, ${match.first_player}, ${match.second_player}, '${match.next_key}', ${match.order})`,
        function (err, result) {
            if (err) {
                console.log(`Error ${err} en la partida ${match.id}`)
            }

        });
}

function setReadyState(id_tournament) {
    connection.query(`update torneo
                        set id_estado = 2
                        where id_torneo = ${id_tournament}  
                    `,
        function (err, result) {
            if (err) {
                console.log(`Error ${err} modificando el estado del torneo`)
            }

        });
}

