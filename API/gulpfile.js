const gulp = require('gulp');
const zip = require('gulp-zip');
 
gulp.task('tournaments',function(){
    return gulp.src('*')
        .pipe(zip('tournament.zip'))
        .pipe(gulp.dest('dist'))
})


gulp.task('default', gulp.series('tournaments'))
